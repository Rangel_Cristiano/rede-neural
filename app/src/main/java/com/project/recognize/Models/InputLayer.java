package com.project.recognize.Models;

import org.ejml.simple.SimpleMatrix;

public class InputLayer {

    private SimpleMatrix mValueMatrix;

    public InputLayer(int numberOfInputs) {
        mValueMatrix = new SimpleMatrix(new double[1][numberOfInputs]);
    }

    public void setValueMatrix(double[][] matrix) {
        mValueMatrix = new SimpleMatrix(matrix);
    }

    public SimpleMatrix getValueMatrix() {
        return mValueMatrix;
    }
}
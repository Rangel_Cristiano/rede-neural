package com.project.recognize.Models;

import org.ejml.simple.SimpleMatrix;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class ConfusionMatrix {

    private final SimpleMatrix matrix;
    private int truePositive;
    private int falsePositive;
    private int trueNegative;
    private int falseNegative;

    public ConfusionMatrix(int numClasses) {
        this.matrix = new SimpleMatrix(numClasses, numClasses);
    }

    public SimpleMatrix getMatrix() {
        return matrix;
    }

    public void exportMatrix(String file) throws IOException {
        this.matrix.saveToFileCSV(file);

        try (PrintStream fileStream = new PrintStream(new FileOutputStream(file, true))) {
            fileStream.println();
            fileStream.print(truePositive);
            fileStream.print(";");
            fileStream.print(falsePositive);
            fileStream.print(";");
            fileStream.print(trueNegative);
            fileStream.print(";");
            fileStream.print(falseNegative);
            fileStream.println();
        }
    }

    public ConfusionMatrix addTruePositive(int c) {
        this.matrix.set(c, c, this.matrix.get(c, c) + 1);
        this.truePositive++;
        return this;
    }

    public ConfusionMatrix addFalsePositive(int c, int trueC) {
        this.matrix.set(trueC, c, this.matrix.get(trueC, c) + 1);
        this.falsePositive++;
        return this;
    }

    public ConfusionMatrix addTrueNegative(int c, int trueC) {
        // TODO nao tem como add na tabela certo?
        this.trueNegative++;
        return this;
    }

    public ConfusionMatrix addFalseNegative(int c, int trueC) {
        this.matrix.set(c, trueC, this.matrix.get(c, trueC) + 1);
        this.falseNegative++;
        return this;
    }

    public int getTruePositive() {
        return truePositive;
    }

    public int getFalsePositive() {
        return falsePositive;
    }

    public int getTrueNegative() {
        return trueNegative;
    }

    public int getFalseNegative() {
        return falseNegative;
    }
}

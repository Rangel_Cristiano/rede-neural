package com.project.recognize.Models;

import com.project.recognize.Utils;

import org.ejml.simple.SimpleMatrix;

public class MiddleLayer {

    private SimpleMatrix mValueMatrix, mErrorsMatrix;
    private Weight mWeightLeft, mWeightRight;

    public MiddleLayer(int numberOfNeurons, Weight weightLeft, Weight weightRight) {
        mValueMatrix = new SimpleMatrix(new double[1][numberOfNeurons]);

        mWeightLeft = weightLeft;
        mWeightRight = weightRight;
    }

    public void setValueMatrixSigmoid(SimpleMatrix matrix) {
        for (int i = 0; i < matrix.numRows(); i++){
            for (int j = 0; j < matrix.numCols(); j++){
                double sigmoid = Utils.sigmoid(matrix.get(i,j));

                matrix.set(i,j, sigmoid);
            }
        }

        mValueMatrix.set(matrix);
    }

    public SimpleMatrix getValueMatrix() {
        return mValueMatrix;
    }

    public void setErrorsMatrix(SimpleMatrix matrix) {
        mErrorsMatrix= new SimpleMatrix(matrix);
    }

    public SimpleMatrix getErrorsMatrix() {
        return mErrorsMatrix;
    }

    public Weight getWeightLeft() {
        return mWeightLeft;
    }

    public void setWeightLeft(Weight weightLeft) {
        mWeightLeft = weightLeft;
    }

    public Weight getWeightRight() {
        return mWeightRight;
    }

    public void setWeightRight(Weight weightRight) {
        mWeightRight = weightRight;
    }
}
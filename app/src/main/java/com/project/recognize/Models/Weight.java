package com.project.recognize.Models;

import org.ejml.simple.SimpleMatrix;

public class Weight {

    private SimpleMatrix mMatrix;

    public Weight(int numberOfInputs, int numberOfNeurons, int initialValue) {

        double[][] initialMatrix = new double[numberOfInputs][numberOfNeurons];
        for (int i = 0; i <numberOfInputs; i++){
            for (int j = 0; j < numberOfNeurons; j++){
                initialMatrix[i][j] = initialValue;
            }
        }
        mMatrix = new SimpleMatrix(initialMatrix);
    }
    
    public void setValueMatrix(SimpleMatrix matrix) {
        mMatrix.set(matrix);
    }

    public SimpleMatrix getValueMatrix() {
        return mMatrix;
    }
}
package com.project.recognize.Models;

public class ConfusionMatrixSum {
    int truePositive, falsePositive, trueNegative, falseNegative;

    public ConfusionMatrixSum() { }

    public int getTruePositive() {
        return truePositive;
    }

    public void addTruePositive() {
        this.truePositive++;
    }

    public int getFalsePositive() {
        return falsePositive;
    }

    public void addFalsePositive() {
        this.falsePositive++;
    }

    public int getTrueNegative() {
        return trueNegative;
    }

    public void addTrueNegative() {
        this.trueNegative++;
    }

    public int getFalseNegative() {
        return falseNegative;
    }

    public void addFalseNegative() {
        this.falseNegative++;
    }
}

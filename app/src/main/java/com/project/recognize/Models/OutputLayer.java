package com.project.recognize.Models;

import com.project.recognize.Utils;

import org.ejml.simple.SimpleMatrix;

public class OutputLayer {

    private SimpleMatrix mValueMatrix, mErrorsMatrix, mExpectedMatrix;
    private ConfusionMatrix confusionMatrix;

    public OutputLayer(int numberOfOutputs) {
        mValueMatrix = new SimpleMatrix(new double[1][numberOfOutputs]);
        confusionMatrix = new ConfusionMatrix(numberOfOutputs);
    }

    public void setValueMatrixSigmoid(SimpleMatrix matrix) {
        for (int i = 0; i < matrix.numRows(); i++){
            for (int j = 0; j < matrix.numCols(); j++){
                double sigmoid = Utils.sigmoid(matrix.get(i,j));

                matrix.set(i,j, sigmoid);
            }
        }

        mValueMatrix.set(matrix);
    }

    public SimpleMatrix getValueMatrix() {
        return mValueMatrix;
    }

    public void setErrorsMatrix(SimpleMatrix matrix) {
        mErrorsMatrix = new SimpleMatrix(matrix);
    }

    public SimpleMatrix getErrorsMatrix() {
        return mErrorsMatrix;
    }

    public SimpleMatrix getExpectedMatrix() {
        return mExpectedMatrix;
    }

    public void setExpectedMatrix(double[][] matrix) {
        mExpectedMatrix = new SimpleMatrix(matrix);
    }

    public ConfusionMatrix getConfusionMatrix() {
        return confusionMatrix;
    }

    public void setConfusionMatrix(ConfusionMatrix confusionMatrix) {
        this.confusionMatrix = confusionMatrix;
    }
}

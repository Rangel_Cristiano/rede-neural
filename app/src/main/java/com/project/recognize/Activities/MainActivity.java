package com.project.recognize.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.project.recognize.Models.InputLayer;
import com.project.recognize.Models.MiddleLayer;
import com.project.recognize.Models.OutputLayer;
import com.project.recognize.Models.Weight;
import com.project.recognize.R;

import org.ejml.simple.SimpleMatrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private InputLayer mInputLayer;
    private OutputLayer mOutputLayer;
    private MiddleLayer mMiddleLayer;
    private int numberOfInputs, numberOfOutputs, numberOfNeurons, initialValuePulse;
    private double mTaxPropagation;

    //Rede neural = Uma grande operação entre matrizes

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberOfInputs = 48;
        numberOfOutputs = 36;
        numberOfNeurons = 120;
        initialValuePulse = 1;
        mTaxPropagation = 0.50;

        initializeLayers();

        final ArrayList<String> dataset = readDataset("dataset.txt");

        int epoch = 0;
        int totalEpoch = 1000;
        while (epoch < totalEpoch) {
            run(dataset, false);

            epoch ++;
        }

        //roda o teste pra gerar minha matriz confusão
        final ArrayList<String> datasetTeste = readDataset("teste.txt");
        run(datasetTeste, true);

        try {
            // no "device file explorer" : /data/data/com.project.recognize/files/confusionMatrix.csv
            this.mOutputLayer.getConfusionMatrix().exportMatrix(getApplicationContext().getFilesDir().getAbsolutePath() + "/confusionMatrix.csv");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void run (ArrayList<String> dataset, boolean isFinalTest) {
        for (int i = 0; i < dataset.size(); i++) {
            final String data = dataset.get(i);

            String input = data.substring(0, numberOfInputs);
            String expected = data.substring(numberOfInputs, numberOfInputs + numberOfOutputs);

            double[][] inputMatrix = new double[1][numberOfInputs];
            for (int l = 0; l < 1; l++) {
                for (int col = 0; col < numberOfInputs - 1; col++) {
                    char charValue = input.charAt(col);

                    inputMatrix[l][col] = Character.getNumericValue(charValue);
                }
            }

            double[][] expectedMatrix = new double[1][numberOfOutputs];
            for (int l = 0; l < 1; l++) {
                for (int col = 0; col < numberOfOutputs - 1; col++) {
                    char charValue = expected.charAt(col);

                    expectedMatrix[l][col] = Character.getNumericValue(charValue);
                }
            }
            /*Random gerador = new Random();
            int randomNumber = gerador.nextInt(100);
            if (randomNumber <= 80) {
                backpropagation();
            }*/


            mOutputLayer.setExpectedMatrix(expectedMatrix);

            backpropagation();

            propagation(inputMatrix);

            //calc a matriz de confusão
            if (isFinalTest) {
                int colExpected = -1, colMaxVal = -1;
                double maxVal = 0;
                for (int l = 0; l < 1; l++) {
                    for (int col = 0; col < mOutputLayer.getExpectedMatrix().numCols(); col++) {
                        if (mOutputLayer.getExpectedMatrix().get(l, col) > colExpected) {
                            colExpected = col;
                        }
                        if (mOutputLayer.getValueMatrix().get(l, col) > maxVal) {
                            colMaxVal = col;
                            maxVal = mOutputLayer.getValueMatrix().get(l, col);
                        }
                    }
                }

                for (int l = 0; l < 1; l++) {
                    for (int col = 0; col < mOutputLayer.getValueMatrix().numCols(); col++) {
                        if (mOutputLayer.getValueMatrix().get(l, col) >= 0.50) { //ligado
                            if (col == colExpected) {
                                mOutputLayer.getConfusionMatrix().addTruePositive(col);
                            } else {
                                mOutputLayer.getConfusionMatrix().addFalsePositive(col, colMaxVal);
                            }
                        } else { //desligado
                            if (col == colExpected) {
                                mOutputLayer.getConfusionMatrix().addFalseNegative(col, colMaxVal);
                            } else {
                                mOutputLayer.getConfusionMatrix().addTrueNegative(col, colMaxVal);
                            }
                        }
                    }
                }
            }
        }
    }

    private ArrayList<String>  readDataset(String pathName) {
        ArrayList<String> list = new ArrayList<>();
        try {
            final InputStreamReader inputStream = new InputStreamReader(getAssets().open(pathName));

            final BufferedReader reader = new BufferedReader(inputStream);
            String line;
            while ((line = reader.readLine()) != null ) {
                list.add(line);
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    private void initializeLayers() {
        mInputLayer = new InputLayer(numberOfInputs);

        Weight pulseleft = new Weight(numberOfInputs, numberOfNeurons, initialValuePulse);
        Weight pulseright = new Weight(numberOfNeurons, numberOfOutputs, initialValuePulse);

        mMiddleLayer = new MiddleLayer(numberOfNeurons, pulseleft, pulseright);

        mOutputLayer = new OutputLayer(numberOfOutputs);
    }

    private void propagation(double[][] inputValues) {
        mInputLayer.setValueMatrix(inputValues);

        mMiddleLayer.setValueMatrixSigmoid(
                mInputLayer.getValueMatrix().mult(
                mMiddleLayer.getWeightLeft().getValueMatrix()));

        mOutputLayer.setValueMatrixSigmoid(
                mMiddleLayer.getValueMatrix().mult(
                mMiddleLayer.getWeightRight().getValueMatrix()));
    }

    private void backpropagation() {
        calcErrors();

        adjustWeights();
    }

    private void calcErrors() {
        mOutputLayer.setErrorsMatrix(
                multElementByElement(
                    multElementByElement(mOutputLayer.getValueMatrix(),
                            createMatrixWithValue(mOutputLayer.getValueMatrix(),1).minus(mOutputLayer.getValueMatrix())),
                    mOutputLayer.getExpectedMatrix().minus(mOutputLayer.getValueMatrix())));

        mMiddleLayer.setErrorsMatrix(
                multElementByElement(
                    multElementByElement(
                        mMiddleLayer.getValueMatrix(),
                            createMatrixWithValue(mMiddleLayer.getValueMatrix(), 1).minus(mMiddleLayer.getValueMatrix())),
                    mOutputLayer.getValueMatrix().mult(mMiddleLayer.getWeightRight().getValueMatrix().transpose())));
    }

    private void adjustWeights() {
        mMiddleLayer.getWeightRight().setValueMatrix(
                mMiddleLayer.getValueMatrix().transpose()
                .mult(
                        multElementByElement(
                                mOutputLayer.getErrorsMatrix(),
                                mMiddleLayer.getWeightRight().getValueMatrix().plus(mTaxPropagation))));

        mMiddleLayer.getWeightLeft().setValueMatrix(
                mInputLayer.getValueMatrix().transpose()
                .mult(
                    multElementByElement(
                            mMiddleLayer.getErrorsMatrix(),
                            mMiddleLayer.getWeightLeft().getValueMatrix().plus(mTaxPropagation))));
    }

    private SimpleMatrix createMatrixWithValue(SimpleMatrix matrix, double value) {
        double[][] matrixResult = new double[matrix.numRows()][matrix.numCols()];

        for (int i = 0; i < matrix.numRows(); i++){
            for (int j = 0; j < matrix.numCols(); j++){
                matrixResult[i][j] = value;
            }
        }

        return new SimpleMatrix(matrixResult);
    }

    private SimpleMatrix multElementByElement(SimpleMatrix matrix1, SimpleMatrix matrix2) {
        double[][] matrix = new double[matrix1.numRows()][matrix1.numCols()];

        for (int i = 0; i < matrix1.numRows(); i++){
            for (int j = 0; j < matrix1.numCols(); j++){
                matrix[i][j] = matrix1.get(i,j) * matrix2.get(i,j);
            }
        }

        return new SimpleMatrix(matrix);
    }
}
